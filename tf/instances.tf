resource "aws_instance" "web-1" {
    ami = "${lookup(var.amis, var.aws_region)}"
    availability_zone = "${var.aws_region}a"
    instance_type = "t2.micro"
    key_name = "${aws_key_pair.ansible.key_name}"
    vpc_security_group_ids = ["${aws_security_group.web.id}"]
    subnet_id = "${aws_subnet.main-public.id}"
    associate_public_ip_address = true
    source_dest_check = false

    tags = {
            Name = "Web Server"
        }



}

